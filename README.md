# Find My Player - Servidor

**Progresso:** CANCELADO<br />
**Autor:** Paulo Victor de Oliveira Leal e Vinicius Luis Lopes Nunes<br />
**Data:** 2017<br />

### Objetivo
Implementar um aplicativo que facilite jogadores de mobas e dentre outros estilos formarem uma equipe para uma partida.

### Observação

IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://dotnet.microsoft.com/)<br />
Banco de dados: [MySQL](https://www.mysql.com/)<br />

### Execução

    $ dotnet restore
    $ dotnet run
    
### Contribuição

Esse projeto não está concluído a livre para consulta.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->

## Conteudo de ajuda
- https://github.com/msx752/RiotGamesApi.AspNetCore -- 
- https://www.nuget.org/packages/RiotGamesApi.AspNetCore/
- URLs para as imagens: https://developer.riotgames.com/static-data.html
- Necessario instalar o XAMPP (https://www.apachefriends.org/pt_br/index.html) e habilitar o mysql e phpmyadmin
- https://imasters.com.br/apis/asp-net-core-criando-uma-aplicacao-mvc-6-web-api-no-vs-2015-community-parte-01/
- https://imasters.com.br/apis/asp-net-core-criando-uma-aplicacao-mvc-6-web-api-no-vs-2015-community-parte-02/
- http://www.macoratti.net/17/05/efcore_mysql1.htm
- http://www.macoratti.net/17/04/efcore_sqlite1.htm