webpackJsonp([28],{

/***/ 731:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home__ = __webpack_require__(904);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_0__home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_0__home__["a" /* HomePage */]),
            ],
        })
    ], HomePageModule);
    return HomePageModule;
}());

//# sourceMappingURL=home.module.js.map

/***/ }),

/***/ 904:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_chat_service__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, viewCtrl, apiUsuario, chatService, plt) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.apiUsuario = apiUsuario;
        this.chatService = chatService;
        this.plt = plt;
        this.isOnBrowser = false;
        this.pages = [
            // { title: 'Main', component: MainPage },
            { title: 'Find my player', component: 'MatchMainPage', gliph: 'people' },
            { title: 'Champion', component: 'ListaChampionsPage', gliph: 'fmp-sword' },
            { title: 'Match', component: 'PartidasPage', gliph: 'fmp-deck' },
            { title: 'Create a team', component: 'FormarTimePage', gliph: 'fmp-team' },
            { title: 'Contacts', component: 'ContatosMainPage', gliph: 'contacts' },
            { title: 'My Profile', component: 'UserProfilePage', gliph: 'person' } //, params: { usuario: this.apiUsuario.getUsuarioLogado(), mostrarFormComentarios: false } },
        ];
        this.isOnBrowser = this.plt.is('mobileweb') || this.plt.is('core');
    }
    HomePage.prototype.ionViewDidLoad = function () {
        if (!this.apiUsuario.getUsuarioLogado().leagueOfLegendsId) {
            //Abrir pagina de cadastro de perfil lol
            this.navCtrl.setRoot('WelcomePage');
            this.navCtrl.push('TelaIntegracaoPage');
        }
        else {
            //Connectar ao servico de chat
            this.chatService.connect(this.apiUsuario.getUsuarioLogado());
            //Ir para pagina 0 (Match)
            this.openPage(this.pages[0]);
        }
    };
    HomePage.prototype.openPage = function (page) {
        this.nav.setRoot(page.component, page.params ? page.params : null);
    };
    HomePage.prototype.logout = function () {
        this.apiUsuario.logout();
        this.navCtrl.setRoot('WelcomePage');
        this.navCtrl.popToRoot();
    };
    HomePage.prototype.isActive = function (page) {
        if (this.nav.getActive() && this.nav.getActive().name === page.component) {
            return 'primary';
        }
        return;
    };
    HomePage.prototype.downloadApp = function () {
        window.open('http://findmyplayer.azurewebsites.net/fmp-app.apk', '_system');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* Nav */])
    ], HomePage.prototype, "nav", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/home/home.html"*/'<!-- <ion-split-pane>\n\n  <ion-menu [content]="content">\n\n    <ion-header>\n\n      <ion-toolbar>\n\n        <ion-title>Menu</ion-title>\n\n      </ion-toolbar>\n\n    </ion-header>\n\n\n\n    <ion-content>\n\n      <ion-list>\n\n        <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n          {{p.title}}\n\n        </button>\n\n      </ion-list>\n\n      <button ion-button block (click)="logout()" class="signup">Logout</button>\n\n    </ion-content>\n\n  </ion-menu>\n\n\n\n  <!-- Disable swipe-to-go-back because it \'s poor UX to combine STGB with side menus -->\n\n  <!-- <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>\n\n</ion-split-pane> -->\n\n\n\n\n\n\n\n<ion-split-pane >\n\n  \n\n    <ion-menu [content]="content">\n\n      <ion-header>\n\n        <ion-toolbar>\n\n          <ion-title>\n\n            Menu\n\n          </ion-title>\n\n        </ion-toolbar>\n\n      </ion-header>\n\n      <ion-content>\n\n        <ion-list>\n\n          <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n              <ion-icon item-start [name]="p.gliph" [color]="isActive(p)"></ion-icon>\n\n            {{p.title}}\n\n          </button>\n\n        </ion-list>\n\n        <button ion-button block (click)="logout()" class="signup">Logout</button>\n\n        <button ion-button block (click)="downloadApp()" clear *ngIf="isOnBrowser" class="button-download-app">Download App</button>\n\n      </ion-content>\n\n    </ion-menu>\n\n    \n\n    <ion-nav [root]="rootPage" #content main></ion-nav>\n\n    \n\n  </ion-split-pane>'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0__providers_chat_service__["a" /* ChatService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* Platform */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ })

});
//# sourceMappingURL=28.js.map