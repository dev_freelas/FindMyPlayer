webpackJsonp([21],{

/***/ 735:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MatchMainPageModule", function() { return MatchMainPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_components_module__ = __webpack_require__(877);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__match_main__ = __webpack_require__(908);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var MatchMainPageModule = /** @class */ (function () {
    function MatchMainPageModule() {
    }
    MatchMainPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__match_main__["a" /* MatchMainPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__match_main__["a" /* MatchMainPage */]),
                __WEBPACK_IMPORTED_MODULE_0__components_components_module__["a" /* ComponentsModule */]
            ],
        })
    ], MatchMainPageModule);
    return MatchMainPageModule;
}());

//# sourceMappingURL=match-main.module.js.map

/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThumbType; });
/* unused harmony export thumbImageUpUrl */
/* unused harmony export thumbImageDownUrl */
/* unused harmony export thumbImageSuperUrl */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ThumbsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_animations__ = __webpack_require__(160);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ThumbType;
(function (ThumbType) {
    ThumbType[ThumbType["UP"] = 0] = "UP";
    ThumbType[ThumbType["DOWN"] = 1] = "DOWN";
    ThumbType[ThumbType["SUPER"] = 2] = "SUPER";
})(ThumbType || (ThumbType = {}));
var thumbImageUpUrl = "assets/imgs/thumb-up.png";
var thumbImageDownUrl = "assets/imgs/thumb-down.png";
var thumbImageSuperUrl = "assets/imgs/thumb-super.png";
/**
 * Generated class for the ThumbsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ThumbsComponent = /** @class */ (function () {
    function ThumbsComponent() {
        console.log("Hello ThumbsComponent Component");
        this.thumbImageAtual = thumbImageUpUrl;
        this.thumbTypeAtual = ThumbType.UP;
    }
    Object.defineProperty(ThumbsComponent.prototype, "thumbType", {
        set: function (value) {
            this._thumbType = value;
            this.mudarImagem(value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ThumbsComponent.prototype, "thumType", {
        get: function () {
            return this._thumbType;
        },
        enumerable: true,
        configurable: true
    });
    ThumbsComponent.prototype.mudarImagem = function (type) {
        switch (type) {
            case ThumbType.DOWN:
                this.thumbImageAtual = thumbImageDownUrl;
                this.thumbTypeAtual = ThumbType.DOWN;
                break;
            case ThumbType.UP:
                this.thumbImageAtual = thumbImageUpUrl;
                this.thumbTypeAtual = ThumbType.UP;
                break;
            case ThumbType.SUPER:
                this.thumbImageAtual = thumbImageSuperUrl;
                this.thumbTypeAtual = ThumbType.SUPER;
                break;
        }
    };
    ThumbsComponent.prototype.animate = function () {
        this.match = true;
    };
    ThumbsComponent.prototype.animationFinished = function (event) {
        this.match = false;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])("thumbType"),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], ThumbsComponent.prototype, "thumbType", null);
    ThumbsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: "thumbs",template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/thumbs/thumbs.html"*/'<!-- Generated template for the ThumbsComponent component -->\n\n<!-- <div *ngIf="mached" [@onMatch]="\'match\'" class="thunbs" (@onMatch.done)="animationFinished($event)" ><img src="assets/imgs/thumb-up.png"/></div>\n\n<div *ngIf="unmached" [@onMatch]="\'match\'" class="thunbs" (@onMatch.done)="animationFinished($event)" ><img src="assets/imgs/thumb-down.png"/></div> -->\n\n<!-- <div>\n\n  {{text}}\n\n</div> -->\n\n\n\n<div *ngIf="match" [@onMatch]="\'match\'" class="thunbs" (@onMatch.done)="animationFinished($event)" ><img src="{{thumbImageAtual}}"/></div>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/thumbs/thumbs.html"*/,
            //inputs: ["thumbType"],
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["k" /* trigger */])("onMatch", [
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["h" /* state */])("match", Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({
                        opacity: "1",
                        transform: "translate(-50%, -50%)"
                    })),
                    Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["j" /* transition */])(":enter", Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["e" /* animate */])("700ms ease-in-out", Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["f" /* keyframes */])([
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({ opacity: 0, transform: "translate(-50%, -40%)" }),
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({ opacity: 1, transform: "translate(-50%, -70%)" }),
                        Object(__WEBPACK_IMPORTED_MODULE_1__angular_animations__["i" /* style */])({ opacity: 1, transform: "translate(-50%, -60%)" })
                    ])))
                ])
            ]
        }),
        __metadata("design:paramtypes", [])
    ], ThumbsComponent);
    return ThumbsComponent;
}());

//# sourceMappingURL=thumbs.js.map

/***/ }),

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__thumbs_thumbs__ = __webpack_require__(872);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__barra_busca_barra_busca__ = __webpack_require__(878);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__emoji_picker_emoji_picker__ = __webpack_require__(879);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__thumbs_thumbs__["b" /* ThumbsComponent */],
                __WEBPACK_IMPORTED_MODULE_3__barra_busca_barra_busca__["a" /* BarraBuscaComponent */],
                __WEBPACK_IMPORTED_MODULE_4__emoji_picker_emoji_picker__["a" /* EmojiPickerComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* IonicModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__thumbs_thumbs__["b" /* ThumbsComponent */],
                __WEBPACK_IMPORTED_MODULE_3__barra_busca_barra_busca__["a" /* BarraBuscaComponent */],
                __WEBPACK_IMPORTED_MODULE_4__emoji_picker_emoji_picker__["a" /* EmojiPickerComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 878:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BarraBuscaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular_components_searchbar_searchbar__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the BarraBuscaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var BarraBuscaComponent = /** @class */ (function () {
    function BarraBuscaComponent() {
        this.debounce = 300;
        this.pesquisar = new __WEBPACK_IMPORTED_MODULE_1__angular_core__["EventEmitter"]();
    }
    BarraBuscaComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.isSearchingExternal = false;
        this.barraBusca.registerOnChange(function () {
            _this.isSearching = true;
        });
    };
    BarraBuscaComponent.prototype.searchItems = function (input) {
        this.isSearching = false;
        this.pesquisar.emit(input.target.value ? input.target.value : "");
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"])(),
        __metadata("design:type", Boolean)
    ], BarraBuscaComponent.prototype, "isSearchingExternal", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Input"])(),
        __metadata("design:type", Number)
    ], BarraBuscaComponent.prototype, "debounce", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Output"])(),
        __metadata("design:type", Object)
    ], BarraBuscaComponent.prototype, "pesquisar", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])("barraBusca"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0_ionic_angular_components_searchbar_searchbar__["a" /* Searchbar */])
    ], BarraBuscaComponent.prototype, "barraBusca", void 0);
    BarraBuscaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: "barra-busca",template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/barra-busca/barra-busca.html"*/'<!-- Generated template for the BarraBuscaComponent component -->\n\n<div>\n\n  <ion-searchbar #barraBusca showCancelButton="false" cancelButtonText="cancel" [debounce]="debounce" placeholder="Search "\n\n  [(ngModel)]="meubb"\n\n    (ionInput)="searchItems($event)">\n\n  </ion-searchbar>\n\n  <div *ngIf="isSearching || isSearchingExternal" class="div-spinner">\n\n    <ion-spinner></ion-spinner>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/barra-busca/barra-busca.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], BarraBuscaComponent);
    return BarraBuscaComponent;
}());

//# sourceMappingURL=barra-busca.js.map

/***/ }),

/***/ 879:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export EMOJI_PICKER_VALUE_ACCESSOR */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmojiPickerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_emoji__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EmojiPickerComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
var EMOJI_PICKER_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["forwardRef"])(function () { return EmojiPickerComponent; }),
    multi: true
};
var EmojiPickerComponent = /** @class */ (function () {
    function EmojiPickerComponent(emojiProvider) {
        this.emojiArr = [];
        this.emojiArr = emojiProvider.getEmojis();
    }
    EmojiPickerComponent.prototype.writeValue = function (obj) {
        this._content = obj;
    };
    EmojiPickerComponent.prototype.registerOnChange = function (fn) {
        this._onChanged = fn;
        this.setValue(this._content);
    };
    EmojiPickerComponent.prototype.registerOnTouched = function (fn) {
        this._onTouched = fn;
    };
    EmojiPickerComponent.prototype.setValue = function (val) {
        this._content += val;
        if (this._content) {
            this._onChanged(this._content);
        }
    };
    EmojiPickerComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'emoji-picker',
            providers: [EMOJI_PICKER_VALUE_ACCESSOR],template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/emoji-picker/emoji-picker.html"*/'<!-- Generated template for the EmojiPickerComponent component -->\n\n<div class="emoji-picker">\n\n  <div class="emoji-items">\n\n    <ion-slides pager>\n\n\n\n      <ion-slide *ngFor="let items of emojiArr">\n\n        <span class="emoji-item"\n\n              (click)="setValue(item)"\n\n              *ngFor="let item of items">\n\n          {{item}}\n\n        </span>\n\n      </ion-slide>\n\n\n\n    </ion-slides>\n\n  </div>\n\n</div>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/components/emoji-picker/emoji-picker.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_emoji__["a" /* EmojiProvider */]])
    ], EmojiPickerComponent);
    return EmojiPickerComponent;
}());

//# sourceMappingURL=emoji-picker.js.map

/***/ }),

/***/ 908:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MatchMainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the MatchMainPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var MatchMainPage = /** @class */ (function () {
    function MatchMainPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tabMatch = 'MatchPage';
        this.tabMeusMatches = 'MeusMatchesPage';
    }
    MatchMainPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad MatchMainPage');
    };
    MatchMainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-match-main',template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/match-main/match-main.html"*/'<!--\n\n  Generated template for the MatchMainPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Find my player</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-tabs>\n\n    <ion-tab tabIcon="fmp-crown" tabTitle="Match" [root]="tabMatch"></ion-tab>\n\n    <ion-tab tabIcon="fmp-sword" tabTitle="Meus Matches" [root]="tabMeusMatches" ></ion-tab>\n\n  </ion-tabs>\n\n</ion-content>'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/match-main/match-main.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* NavParams */]])
    ], MatchMainPage);
    return MatchMainPage;
}());

//# sourceMappingURL=match-main.js.map

/***/ })

});
//# sourceMappingURL=21.js.map