webpackJsonp([22],{

/***/ 738:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NovoGrupoModalPageModule", function() { return NovoGrupoModalPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__novo_grupo_modal__ = __webpack_require__(919);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NovoGrupoModalPageModule = /** @class */ (function () {
    function NovoGrupoModalPageModule() {
    }
    NovoGrupoModalPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__novo_grupo_modal__["a" /* NovoGrupoModalPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__novo_grupo_modal__["a" /* NovoGrupoModalPage */]),
            ],
        })
    ], NovoGrupoModalPageModule);
    return NovoGrupoModalPageModule;
}());

//# sourceMappingURL=novo-grupo-modal.module.js.map

/***/ }),

/***/ 882:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatGrupo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__BaseModel__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var ChatGrupo = /** @class */ (function (_super) {
    __extends(ChatGrupo, _super);
    function ChatGrupo(init) {
        var _this = _super.call(this, __WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */].injector.get(__WEBPACK_IMPORTED_MODULE_1__providers_sessao_sessao__["a" /* SessaoProvider */])) || this;
        Object.assign(_this, init);
        return _this;
    }
    ChatGrupo.prototype.getToJsonProperties = function () {
        return [
            'nome',
            'privado',
            'chatUsuarios',
            'elo'
        ];
    };
    return ChatGrupo;
}(__WEBPACK_IMPORTED_MODULE_0__BaseModel__["a" /* BaseModel */]));

//# sourceMappingURL=ChatGrupo.js.map

/***/ }),

/***/ 919:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NovoGrupoModalPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_ChatGrupo__ = __webpack_require__(882);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NovoGrupoModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NovoGrupoModalPage = /** @class */ (function () {
    function NovoGrupoModalPage(navCtrl, navParams, modal, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modal = modal;
        this.viewCtrl = viewCtrl;
        this.grupo = new __WEBPACK_IMPORTED_MODULE_0__models_ChatGrupo__["a" /* ChatGrupo */]();
    }
    NovoGrupoModalPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NovoGrupoModalPage');
    };
    NovoGrupoModalPage.prototype.clickCancel = function () { this.viewCtrl.dismiss(); };
    NovoGrupoModalPage.prototype.clickOk = function () {
        if (!this.grupo.nome || !this.grupo.elo) {
            alert("Check empty fiedls");
            return;
        }
        this.viewCtrl.dismiss(this.grupo);
    };
    NovoGrupoModalPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'page-novo-grupo-modal',template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/novo-grupo-modal/novo-grupo-modal.html"*/'<!--\n\n  Generated template for the NovoGrupoModalPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n\n\n<ion-content padding>\n\n  <ion-list>\n\n    <ion-item>\n\n      <ion-label floating>Name</ion-label>\n\n      <ion-input type="text" [(ngModel)]="grupo.nome"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label floating>Elo</ion-label>\n\n      <ion-input type="text" [(ngModel)]="grupo.elo"></ion-input>\n\n    </ion-item>\n\n    <ion-item>\n\n      <ion-label>Privado</ion-label>\n\n      <ion-toggle [(ngModel)]="grupo.privado"></ion-toggle>\n\n    </ion-item>\n\n  </ion-list>\n\n\n\n  <div class="alert-button-group" ng-reflect-klass="alert-button-group" >\n\n    <!--bindings={\n\n    "ng-reflect-ng-for-of": "[object Object],[object Object"\n\n  }-->\n\n    <button ion-button outline (click)="clickCancel()">\n\n      <span class="button-inner">Cancel</span>\n\n      <div class="button-effect"></div>\n\n    </button>\n\n    <button ion-button outline (click)="clickOk()" >\n\n      <span class="button-inner">Ok</span>\n\n      <div class="button-effect"></div>\n\n    </button>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/novo-grupo-modal/novo-grupo-modal.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ViewController */]])
    ], NovoGrupoModalPage);
    return NovoGrupoModalPage;
}());

//# sourceMappingURL=novo-grupo-modal.js.map

/***/ })

});
//# sourceMappingURL=22.js.map