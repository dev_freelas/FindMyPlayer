webpackJsonp([25],{

/***/ 741:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterPageModule", function() { return RegisterPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register__ = __webpack_require__(922);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegisterPageModule = /** @class */ (function () {
    function RegisterPageModule() {
    }
    RegisterPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__register__["a" /* RegisterPage */]),
            ],
        })
    ], RegisterPageModule);
    return RegisterPageModule;
}());

//# sourceMappingURL=register.module.js.map

/***/ }),

/***/ 922:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_Usuario__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams, formBuider, apiUsuario, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuider = formBuider;
        this.apiUsuario = apiUsuario;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.passwordMismatch = false;
        this.emailMismatch = false;
        this.registro = new __WEBPACK_IMPORTED_MODULE_0__models_Usuario__["a" /* Usuario */]();
        this.registerForm = this.formBuider.group({
            nome: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            email: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            emailConfirmation: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].email, __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            password: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(6), __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]],
            passwordConfirmation: ['', [__WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].minLength(6), , __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]]
        });
    }
    RegisterPage.prototype.validateEqual = function (referencia) {
        return function (control) {
            // self value
            var v = control.value;
            // value not equal
            if (referencia && v !== referencia) {
                return {
                    validateEqual: false
                };
            }
            return {
                validateEqual: true
            };
        };
    };
    RegisterPage.prototype.ionViewDidLoad = function () {
    };
    RegisterPage.prototype.register = function (usuario) {
        var _this = this;
        //this.navCtrl.push(TelaIntegracaoPage, this.registro);
        var loading = this.loadingCtrl.create({ content: 'Please Wait...' });
        loading.present();
        this.apiUsuario.addUsuario(usuario).subscribe(function (usuario) {
            console.log("Registrado:");
            console.log(usuario);
            _this.navCtrl.pop(); // push(TelaIntegracaoPage, usuario);
            loading.dismiss();
        }, function (erro) {
            console.log("Erro ao registrar:");
            console.log(erro);
            var alert = _this.alertCtrl.create({
                title: 'Fail to register',
                buttons: ['OK']
            });
            alert.present();
            loading.dismiss();
        });
    };
    RegisterPage.prototype.registerFormSubmit = function () {
        this.registerForm.updateValueAndValidity();
        if (this.registerForm.valid && this.verificarPasswordIguais() && this.verificarEmailsIguals()) {
            var usuarioSalvar = new __WEBPACK_IMPORTED_MODULE_0__models_Usuario__["a" /* Usuario */]({
                nome: this.registerForm.controls.nome.value,
                email: this.registerForm.controls.email.value,
                senhaHash: this.registerForm.controls.password.value,
                tipoAtivacaoId: 1,
            });
            this.register(usuarioSalvar);
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Check invalid fields',
                buttons: ['OK']
            });
            alert_1.present();
        }
    };
    RegisterPage.prototype.validarCamposNaoValidados = function (input) {
        switch (input.ngControl.name) {
            case "passwordConfirmation":
                this.verificarPasswordIguais();
                break;
            case "emailConfirmation":
                this.verificarEmailsIguals();
                break;
        }
    };
    RegisterPage.prototype.verificarEmailsIguals = function () {
        if (this.registro.emailConfirmation != this.registro.email) {
            this.registerForm.controls.emailConfirmation.markAsDirty();
            this.registerForm.controls.emailConfirmation.setErrors({ "mismatch": true });
            this.emailMismatch = true;
            return false;
        }
        return true;
    };
    RegisterPage.prototype.verificarPasswordIguais = function () {
        if (this.registro.passwordConfirmation != this.registro.password) {
            this.registerForm.controls.passwordConfirmation.markAsDirty();
            this.registerForm.controls.passwordConfirmation.setErrors({ "mismatch": true });
            this.passwordMismatch = true;
            return false;
        }
        return true;
    };
    RegisterPage.prototype.onFocusOut = function (evento) {
        this.validarCamposNaoValidados(evento);
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/register/register.html"*/'<!--\n\n  Generated template for the RegisterPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>Register</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form [formGroup]="registerForm" (ngSubmit)="registerFormSubmit()">\n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-label floating>name</ion-label>\n\n        <ion-input type="text" formControlName="nome" [(ngModel)]="this.registro.nome"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="invalid-message" *ngIf="registerForm.controls.nome.errors && (registerForm.controls.nome.dirty || registerForm.controls.nome.touched )">\n\n        <p [hidden]="!registerForm.controls.nome.errors.required">this field is required</p>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>e-mail</ion-label>\n\n        <ion-input type="email" formControlName="email" [(ngModel)]="this.registro.email"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="invalid-message" *ngIf="registerForm.controls.email.errors && (registerForm.controls.email.dirty || registerForm.controls.email.touched )">\n\n        <p [hidden]="!registerForm.controls.email.errors.email">invalid email</p>\n\n        <p [hidden]="!registerForm.controls.email.errors.required">this field is required</p>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>confirm e-mail</ion-label>\n\n        <ion-input type="email" #emailConfirmation formControlName="emailConfirmation" [(ngModel)]="this.registro.emailConfirmation"\n\n          (focusout)="onFocusOut(emailConfirmation)"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="invalid-message" *ngIf="registerForm.controls.emailConfirmation.errors && (registerForm.controls.emailConfirmation.dirty || registerForm.controls.emailConfirmation.touched )">\n\n        <p [hidden]="!registerForm.controls.emailConfirmation.errors.email">invalid email</p>\n\n        <p [hidden]="!registerForm.controls.emailConfirmation.errors.required">this field is required</p>\n\n        <p [hidden]="!emailMismatch">email mismatch</p>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>password</ion-label>\n\n        <ion-input type="password" formControlName="password" [(ngModel)]="registro.password"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="invalid-message" *ngIf="registerForm.controls.password.errors && (registerForm.controls.password.dirty || registerForm.controls.password.touched )">\n\n        <p [hidden]="!registerForm.controls.password.errors.password">invalid password</p>\n\n        <p [hidden]="!registerForm.controls.password.errors.required">this field is required</p>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-label floating>confirm password</ion-label>\n\n        <ion-input type="password" #passwordConfirmation formControlName="passwordConfirmation" [(ngModel)]="this.registro.passwordConfirmation"\n\n          (focusout)="onFocusOut(passwordConfirmation)"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="invalid-message" *ngIf="registerForm.controls.passwordConfirmation.errors && (registerForm.controls.passwordConfirmation.dirty || registerForm.controls.passwordConfirmation.touched )">\n\n        <p [hidden]="!registerForm.controls.passwordConfirmation.errors.passwordConfirmation">invalid passwordConfirmation</p>\n\n        <p [hidden]="!registerForm.controls.passwordConfirmation.errors.required">this field is required</p>\n\n        <p [hidden]="!passwordMismatch">password mismatch</p>\n\n      </ion-item>\n\n      <div padding>\n\n        <button type="" ion-button style="float: right">Register</button>\n\n      </div>\n\n    </ion-list>\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/register/register.html"*/,
            providers: [__WEBPACK_IMPORTED_MODULE_1__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */]]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ })

});
//# sourceMappingURL=25.js.map