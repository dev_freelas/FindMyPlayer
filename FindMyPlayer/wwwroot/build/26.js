webpackJsonp([26],{

/***/ 739:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartidasPageModule", function() { return PartidasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__partidas__ = __webpack_require__(920);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PartidasPageModule = /** @class */ (function () {
    function PartidasPageModule() {
    }
    PartidasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__partidas__["a" /* PartidasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__partidas__["a" /* PartidasPage */]),
            ],
        })
    ], PartidasPageModule);
    return PartidasPageModule;
}());

//# sourceMappingURL=partidas.module.js.map

/***/ }),

/***/ 920:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PartidasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_usuario_service_usuario_service__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_leagueoflegends_service_leagueoflegends_service__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PartidasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PartidasPage = /** @class */ (function () {
    function PartidasPage(navCtrl, navParams, apiLol, apiUsuario) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiLol = apiLol;
        this.apiUsuario = apiUsuario;
    }
    PartidasPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad PartidasPage');
        this.apiLol.getEspectador(this.apiUsuario.getUsuarioLogado().leagueOfLegendsId)
            .subscribe(function (resp) {
            _this.partida = resp;
            console.log(resp);
        });
    };
    PartidasPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        this.apiLol.getEspectador(this.apiUsuario.getUsuarioLogado().leagueOfLegendsId)
            .subscribe(function (resp) {
            _this.partida = resp;
            refresher.complete();
        }, function (err) { return console.log('Erro ao carreggar partidas'); });
    };
    PartidasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
            selector: 'page-partidas',template:/*ion-inline-start:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/partidas/partidas.html"*/'<!--\n\n  Generated template for the PartidasPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>partidas</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content>\n\n      <div *ngIf="!partida">\n\n        No matches available\n\n      </div>\n\n      <div *ngIf="partida">\n\n        <ion-card>\n\n          <ion-card-content>\n\n            Match: {{partida.gameType}}\n\n          </ion-card-content>\n\n        </ion-card>\n\n        <ion-grid>\n\n          <ion-row>\n\n            <ion-col col-12 col-md-6>\n\n              <ion-list>\n\n                <ion-item *ngFor="let jogador of jogadores" (click)="verPerfil()">\n\n                  <ion-card>\n\n                    <ion-card-content>\n\n                      <h2>Summoner</h2>\n\n                      <h3>{{jogador.summonerId}}</h3>\n\n                      <div>\n\n                        <span>\n\n                          <h2>Summoner</h2>\n\n                          <h3>{{jogador.champion}}</h3>\n\n                        </span>\n\n                        <span>\n\n                          <h2>Summoner</h2>\n\n                          <h3>{{jogador.summonerId}}</h3>\n\n                        </span>\n\n                      </div>\n\n                      <h2>K/D/A</h2>\n\n                      <h3>{{jogador.kills}}/{{jogador.deaths}}/{{jogador.assistence}}</h3>\n\n                    </ion-card-content>\n\n                  </ion-card>\n\n                </ion-item>\n\n              </ion-list>\n\n            </ion-col>\n\n            <ion-col col-12 col-md-6>\n\n\n\n            </ion-col>\n\n          </ion-row>\n\n        </ion-grid>\n\n\n\n      </div>\n\n    </ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/media/vinih/564617C4783ECA80/Arquivos/Projetos/FMP/Front/src/pages/partidas/partidas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__providers_leagueoflegends_service_leagueoflegends_service__["a" /* LeagueoflegendsServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_0__providers_usuario_service_usuario_service__["a" /* UsuarioServiceProvider */]])
    ], PartidasPage);
    return PartidasPage;
}());

//# sourceMappingURL=partidas.js.map

/***/ })

});
//# sourceMappingURL=26.js.map