﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    public class Base {
        [NotMapped]
        public string Versao {
            get { return "1.0"; }
            set { Versao = value; }
        }
        [NotMapped]
        private string status = "OK";
        [NotMapped]
        public string Status {
            get { return status; }
            set { status = value; }
        }

        [NotMapped]
        public string ChaveSessao { get; set; }

        [NotMapped]
        public DateTime TimeStamp { get; set; }

        public static bool ValidarChave( int usuarioId, string chave, DateTime time ) {
            
            using(EntidadesContexto _context = new EntidadesContexto()) {
                var sessao = _context.SessasoUsuario.SingleOrDefault( a => a.UsuarioId == usuarioId );
                if(sessao != null && sessao.ChaveSessao.Equals(chave) && DateTime.Now.Subtract( sessao.TimeStamp ).TotalMilliseconds < 15 * 60 * 1000) {
                    sessao.TimeStamp = DateTime.Now;
                    return true;
                }
                _context.SaveChanges();
            }
            return false;
        }


    }


}
