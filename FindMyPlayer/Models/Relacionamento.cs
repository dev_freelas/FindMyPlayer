﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_relacionamento" )]
    public class Relacionamento : Base {
        [Key, Column( "relacionamento_id" )]
        public int RelacionamentoId { get; set; }

        [Column( "usuario1_id" )]
        public int Usuario1Id { get; set; }
        public Usuario Usuario1 { get; set; }

        [Column( "tipo_relacionamento_id" )]
        public int TipoRelacionamentoId { get; set; }
        public TipoRelacionamento TipoRelacionamento { get; set; }

        [Column( "usuario2_id" )]
        public int Usuario2Id { get; set; }
        public Usuario Usuario2 { get; set; }

        [Column("data_hora")]
        public DateTime DataHora { get; set; }
    }
}
