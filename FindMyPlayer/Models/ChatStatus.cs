﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_chat_status" )]
    public class ChatStatus : Base {

        [Key, Column( "chat_status_id" )]
        public int ChatStatusId { get; set; }

        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
