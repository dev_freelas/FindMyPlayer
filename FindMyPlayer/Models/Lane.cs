﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_lane" )]
    public class Lane : Base {
        [Key, Column( "lane_id" )]
        public int LaneId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
