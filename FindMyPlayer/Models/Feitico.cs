﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_feitico" )]
    public class Feitico : Base {
        [Key, Column( "feitico_id" )]
        public int FeiticoId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
        [Column( "descricao" )]
        public string Descricao { get; set; }
        [Column( "imagem" )]
        public string Imagem { get; set; }

        [Column( "jogo_id" )]
        public int JogoId { get; set; }
        public Jogo Jogo { get; set; }
    }
}
