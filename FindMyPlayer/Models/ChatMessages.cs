﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_chat_message" )]
    public class ChatMessages : Base{
        [Key, Column( "chat_message_id" )]
        public int ChatId { get; set; }

        [Column("usuario1_id")]
        public int Usuario1Id { get; set; }
        public Usuario Usuario1 { get; set; }

        [Column( "usuario2_id" )]
        public int? Usuario2Id { get; set; }
        public Usuario Usuario2 { get; set; }

        [Column( "chat_grupo_id" )]
        public int? ChatGrupoId { get; set; }
        public ChatGrupo ChatGrupo { get; set; }

        [Column( "chat_status_id" )]
        public int? ChatStatusId { get; set; }
        public ChatStatus ChatStatus { get; set; }

        [Column( "message" )]
        public string Message { get; set; }

        [Column( "time" )]
        public DateTime Time { get; set; }
    }
}
