﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_build" )]
    public class Build : Base{
        [Key, Column( "build_id" )]
        public int BuildId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
        [Column( "descricao" )]
        public string Descricao { get; set; }

        [Column("heroi_id")]
        public int HeroiId { get; set; }
        public Heroi Heroi { get; set; }

        [Column( "usuario_id" )]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        public List<Comentario> Comentarios { get; set; }
    }
}
