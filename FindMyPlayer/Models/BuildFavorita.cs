﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {

    [Table("tb_build_favorita")]
    public class BuildFavorita {
        [Key, Column("build_favorita_id")]
        public int BuildFavoritaId { get; set; }

        [Column("build_id")]
        public int BuildId { get; set; }
        public Build Build { get; set; }

        [Column("usuario_id")]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
    }
}
