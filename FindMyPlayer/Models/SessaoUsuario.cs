﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_sessao_usuario")]
    public class SessaoUsuario : Base{
        [Key, Column("sessao_usuario_id")]
        public int SessaoUsuarioId { get; set; }

        [Column("usuario_id")]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        [Column("time_stamp")]
        public DateTime TimeStamp { get; set; }

        [Column("chave_sessao")]
        public string ChaveSessao { get; set; }
    }
}
