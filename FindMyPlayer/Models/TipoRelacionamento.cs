﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_tipo_relacionamento" )]
    public class TipoRelacionamento : Base {
        [Key, Column( "tipo_relacionamento_id" )]
        public int TipoRelacionamentoId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
