﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_amizade")]
    public class Amizade : Base {
        [Key, Column( "amizade_id" )]
        public int AmizadeId { get; set; }

        [Column("usuario1")]
        public int Usuario1Id { get; set; }
        public Usuario Usuario1 { get; set; }

        [Column( "usuario2" )]
        public int Usuario2Id { get; set; }
        public Usuario Usuario2 { get; set; }

    }
}
