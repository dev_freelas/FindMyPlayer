﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {

    public class Error : Base {
        [NotMapped]
        public int Codigo { get; set; }
        [NotMapped]
        public string Mensagem { get; set; }

        public override string ToString() {
            return JsonConvert.SerializeObject( this );
        }
    }
}

