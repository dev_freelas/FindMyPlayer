﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_jogo" )]
    public class Jogo : Base {
        [Key, Column( "jogo_id" )]
        public int JogoId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
