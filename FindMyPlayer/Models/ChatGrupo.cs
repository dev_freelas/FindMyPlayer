﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_chat_grupo")]
    public class ChatGrupo {
        [Key, Column("chat_grupo_id")]
        public int ChatGrupoId { get; set; }

        [Column("nome")]
        public string Nome { get; set; }

        [Column("privado")]
        public bool Privado { get; set; }

        [Column("Elo")]
        public string Elo { get; set; }

        List<ChatGrupoUsuario> ChatUsuarios { get; set; }
    }
}
