﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_build_item" )]
    public class BuildItem : Base {
        [Key, Column( "build_item_id" )]
        public int BuildItemId { get; set; }

        [Column( "item_id" )]
        public int ItemId { get; set; }
        public Item Item { get; set; }

        [Column( "build_id" )]
        public int BuildId { get; set; }
        public Build Build { get; set; }
    }
}
