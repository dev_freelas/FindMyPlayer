﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models.DTO {

    public class RankPositions {
        public string Nome { get; set; }
        public string Rank { get; set; }
        public string Tier { get; set; }
        public int Vitorias { get; set; }
        public int Derrotas { get; set; }
    }

    public class DadosNaoEstaticosUsuario {
        public double DesempenhoRecente { get; set; }
        public Dictionary<string, double> DesempenhoClasse { get; set; }
        public int RankInterno { get; set; }
        public List<RankPositions> RankPositions { get; set; }

        public DadosNaoEstaticosUsuario() {
            DesempenhoClasse = new Dictionary<string, double>();
            RankPositions = new List<DTO.RankPositions>();
        }
    }
}
