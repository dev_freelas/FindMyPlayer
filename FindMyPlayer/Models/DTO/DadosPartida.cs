﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models.DTO {
    public class Jogador {
        public int SummonerId { get; set; }

        public int ChampionId { get; set; }
        public String Champion { get; set; }

        public int ChampionHistory { get; set; }

        public int Kills { get; set; }
        public int Deaths { get; set; }
        public int Assistence { get; set; }

        public int Farm { get; set; }

    }

    public class DadosPartida {
        public List<Jogador> Jogadores;
        public string GameType { get; set; }
        public int MapId { get; set; }
    }
}
