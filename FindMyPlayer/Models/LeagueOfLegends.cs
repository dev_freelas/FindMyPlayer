﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_league_of_legends" )]
    public class LeagueOfLegends : Base {
        [Key, Column( "league_of_legends_id" )]
        public int LeagueOfLegendsId { get; set; }

        [Column( "summoner_id" )]
        public int SummonerId { get; set; }

        [Column("account_id")]
        public int AccountId { get; set; }

        [Column( "nick" )]
        public string Nick { get; set; }
        [Column( "level" )]
        public int Level { get; set; }
        [Column( "imagem" )]
        public string Imagem { get; set; }

        [Column( "regiao_id" )]
        public int RegiaoId { get; set; }
        public LeagueOfLegends_Regiao Regiao { get; set; }

        [Column( "heroi_principal_id" )]
        public int HeroiPrincipalId { get; set; }
        public Heroi HeroiPrincipal { get; set; }

        [Column( "heroi_secundario_id" )]
        public int HeroiSecundarioId { get; set; }
        public Heroi HeroiSecundario { get; set; }

        [Column( "lane_principal_id" )]
        public int LanePrincipalId { get; set; }
        public Lane LanePrincipal { get; set; }

        [Column( "lane_secundaria_id" )]
        public int LaneSecundariaId { get; set; }
        public Lane LaneSecundaria { get; set; }

        [Column( "numero_vitorias" )]
        public int NumeroVitorias { get; set; }

    }
}
