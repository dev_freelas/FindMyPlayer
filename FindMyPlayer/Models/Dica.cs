﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_dica" )]
    public class Dica : Base {
        [Key, Column( "dica_id" )]
        public int DicaId { get; set; }
        [Column( "descricao" )]
        public int Descricao { get; set; }

        [Column( "heroi_id" )]
        public int HeroiId { get; set; }
        public Heroi Heroi { get; set; }

        [Column( "usuario_id" )]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }
    }
}
