﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table("tb_counter")]
    public class Counter : Base{
        [Key, Column( "counter_id" )]
        public int CounterId { get; set; }

        [Column("heroi_id")]
        public int HeroiId { get; set; }
        public Heroi Heroi { get; set; }

        [Column("heroi_inimigo_id")]
        public int HeroiInimigoId { get; set; }
        public Heroi HeroiInimigo { get; set; }
    }
}
