﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_comentarios" )]
    public class Comentario : Base {
        [Key, Column( "comentarios_id" )]
        public int ComentarioId { get; set; }
        [Column( "descricao" )]
        public int Descricao { get; set; }

        [Column( "usuario_id" )]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }

        [Column( "build_id" )]
        public int BuildId { get; set; }
        public Build Build { get; set; }
    }
}
