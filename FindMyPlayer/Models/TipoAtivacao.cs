﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_tipo_ativacao" )]
    public class TipoAtivacao : Base {
        [Key, Column( "tipo_ativacao_id" )]
        public int TipoAtivacaoId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
    }
}
