﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_heroi" )]
    public class Heroi : Base{
        [Key, Column( "heroi_id" )]
        public int HeroiId { get; set; }
        [Column( "nome" )]
        public string Nome { get; set; }
        [Column("historia")]
        public string Historia { get; set; }
        [Column("titulo")]
        public string Titulo { get; set; }
        [Column("imagem")]
        public string Imagem { get; set; }
        [Column("champion_id")]
        public int ChampionId { get; set; } 

        [Column("jogo_id")]
        public int JogoId { get; set; }
        public Jogo Jogo { get; set; }

        public List<Dica> Dicas { get; set; }
    }
}
