﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace FindMyPlayer.Models {
    [Table( "tb_build_runa" )]
    public class BuildRuna : Base{
        [Key, Column( "build_runa_id" )]
        public int BuildRunaId { get; set; }

        [Column( "runa_id" )]
        public int RunaId { get; set; }
        public Runa Runa { get; set; }

        [Column( "build_id" )]
        public int BuildId { get; set; }
        public Build Build { get; set; }
    }
}
