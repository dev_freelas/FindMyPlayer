﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FindMyPlayer.Models;
using FindMyPlayer.Controllers.Utils;
using RiotGamesApi;
using RiotGamesApi.AspNetCore;
using Microsoft.Extensions.Logging;
using RiotGamesApi.AspNetCore.Extensions;
using System.Net.WebSockets;
using Microsoft.AspNetCore.Http;
using System.Threading;
using WebSocketManager;
using ChatApplication;
using FindMyPlayer.Controllers.Utils.WebSocketManager;

namespace FindMyPlayer {
    public class Startup {
        public Startup(IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services) {

            services.AddCors( options => {
                options.AddPolicy( "AllowAll",
                    builder => {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    } );
            } );
            
            services.AddMvc();
            services.AddLeagueOfLegendsApi( LeagueOfLegendsAPI.APIKEY(), (cache) => {
                //overrides default values
                cache.EnableStaticApiCaching = true;
                // cache.StaticApiCacheExpiry = new TimeSpan( 1, 0, 0 );


                //custom caching is activated
                //working for any api except static-api
                //  cache.EnableCustomApiCaching = true;
                //summoner-profiles are cached for 5sec
                //  cache.AddCacheRule( LolUrlType.NonStatic, LolApiName.Summoner,
                //                   new TimeSpan( 0, 0, 5 ) );
                //
                return cache;
            } );

            

            services.AddWebSocketManager();

            return services.BuildServiceProvider();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            } else {
                app.UseExceptionHandler( "/Home/Error" );
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors( "AllowAll" );
            //app.UseMvc( routes => {
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}" );
            //} );

            app.UseMvc();

            app.UseRiotGamesApi();

            app.UseWebSockets();

            app.MapWebSocketManager( "/ws", serviceProvider.GetService<NotificationsMessageHandler>() );
            //app.MapWebSocketManager( "/test", serviceProvider.GetService<TestMessageHandler>() );

            app.Run( async (context) =>            {
                context.Response.Headers.Add( "Access-Control-Allow-Origin", "" );
                context.Response.Headers.Add( "Access-Control-Allow-Headers", "" );
                context.Response.Headers.Add( "Access-Control-Allow-Methdos", "*" );
            } );


            using (var context = new EntidadesContexto()) {
                InicializaBD.InitializeAsync( );
            }
        }
    }
}
