using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/LeagueOfLegends_Regioes" )]
    public class LeagueOfLegends_RegioesController : Controller {
        private readonly EntidadesContexto _context;

        public LeagueOfLegends_RegioesController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/LeagueOfLegends_Regioes
        [HttpGet]
        public IEnumerable<LeagueOfLegends_Regiao> GetLeagueOfLegends_Regiao() {
            return _context.LeagueOfLegends_Regiao;
        }

        // GET: api/LeagueOfLegends_Regioes/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetLeagueOfLegends_Regiao( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var leagueOfLegends_Regiao = await _context.LeagueOfLegends_Regiao.SingleOrDefaultAsync( m => m.LeagueOfLegends_RegiaoId == id );

            if (leagueOfLegends_Regiao == null) {
                return NotFound( );
            }

            return Ok( leagueOfLegends_Regiao );
        }

        // PUT: api/LeagueOfLegends_Regioes/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutLeagueOfLegends_Regiao( [FromRoute] int id, [FromBody] LeagueOfLegends_Regiao leagueOfLegends_Regiao ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != leagueOfLegends_Regiao.LeagueOfLegends_RegiaoId) {
                return BadRequest( );
            }

            _context.Entry( leagueOfLegends_Regiao ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!LeagueOfLegends_RegiaoExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/LeagueOfLegends_Regioes
        [HttpPost]
        public async Task<IActionResult> PostLeagueOfLegends_Regiao( [FromBody] LeagueOfLegends_Regiao leagueOfLegends_Regiao ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.LeagueOfLegends_Regiao.Add( leagueOfLegends_Regiao );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetLeagueOfLegends_Regiao", new { id = leagueOfLegends_Regiao.LeagueOfLegends_RegiaoId }, leagueOfLegends_Regiao );
        }

        // DELETE: api/LeagueOfLegends_Regioes/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteLeagueOfLegends_Regiao( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var leagueOfLegends_Regiao = await _context.LeagueOfLegends_Regiao.SingleOrDefaultAsync( m => m.LeagueOfLegends_RegiaoId == id );
            if (leagueOfLegends_Regiao == null) {
                return NotFound( );
            }

            _context.LeagueOfLegends_Regiao.Remove( leagueOfLegends_Regiao );
            await _context.SaveChangesAsync( );

            return Ok( leagueOfLegends_Regiao );
        }

        private bool LeagueOfLegends_RegiaoExists( int id ) {
            return _context.LeagueOfLegends_Regiao.Any( e => e.LeagueOfLegends_RegiaoId == id );
        }
    }
}