﻿using FindMyPlayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FindMyPlayer.Controllers.Utils {
    public class Procedimentos {
        public static void AtualizarRankInterno() {
            Task.Run( async () => {
                using(var _context = new EntidadesContexto()) {
                    foreach(var lol in _context.LeagueOfLegends) {
                        var ultimasVitorias = await LeagueOfLegendsAPI.RecuperarVitoriasAsync( LeagueOfLegendsAPI.RetornarRegiao( lol.RegiaoId ), lol.AccountId, lol.SummonerId );
                        lol.NumeroVitorias = lol.NumeroVitorias + ultimasVitorias;
                    }
                    _context.SaveChanges();
                }
                var dataAgora = new DateTime( 1, 1, 1, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second );
                var dataMarcada = new DateTime( 1, 1, 1, 3, 30, 0 );
                double time = 0L;
                time = new TimeSpan( dataMarcada.Subtract( dataAgora ).Ticks ).TotalMilliseconds;
                Thread.Sleep( Convert.ToInt32( Math.Abs( time ) ) );
            } );
        }
    }
}
