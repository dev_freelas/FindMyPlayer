using FindMyPlayer.Controllers.Utils;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;

namespace WebSocketManager {
    public class WebSocketConnectionManager {
        private ConcurrentDictionary<string, WebSocket> _sockets = new ConcurrentDictionary<string, WebSocket>();

        public WebSocket GetSocketById(string id) {
            return _sockets.FirstOrDefault( p => p.Key == id ).Value;
        }

        public ConcurrentDictionary<string, WebSocket> GetAll() {
            return _sockets;
        }

        public string GetId(WebSocket socket) {
            return _sockets.FirstOrDefault( p => p.Value == socket ).Key;
        }

        public void AddSocket(WebSocket socket, string usuarioId = "") {
            _sockets.TryAdd( usuarioId.Equals("") ? CreateConnectionId() : usuarioId, socket );

            Caminhos.usuariosConectados = _sockets.Keys.ToList();
            Console.WriteLine( "Usuario " + usuarioId + " conectado as "+ DateTime.Now );
        }

        public async Task RemoveSocket(string id) {
            WebSocket socket;
            _sockets.TryRemove( id, out socket );

            await socket.CloseAsync( closeStatus: WebSocketCloseStatus.NormalClosure,
                                    statusDescription: "Closed by the WebSocketManager",
                                    cancellationToken: CancellationToken.None );
            Caminhos.usuariosConectados = _sockets.Keys.ToList();

            Console.WriteLine( "Usuario " + id + " desconectou as " + DateTime.Now );
        }

        private string CreateConnectionId() {
            return Guid.NewGuid().ToString();
        }
    }
}
