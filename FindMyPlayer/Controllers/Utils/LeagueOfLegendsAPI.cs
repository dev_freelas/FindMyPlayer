﻿using RiotGamesApi.AspNetCore;
using RiotGamesApi;
using System;
using System.Collections.Generic;
using RiotGamesApi.AspNetCore.RiotApi.NonStaticEndPoints.Summoner;
using RiotGamesApi.AspNetCore.RiotApi.Enums;
using RiotGamesApi.AspNetCore.RiotApi.StaticEndPoints.Items;
using RiotGamesApi.AspNetCore.Models;
using RiotGamesApi.AspNetCore.RiotApi.NonStaticEndPoints.Spectator;
using RiotGamesApi.AspNetCore.RiotApi.StaticEndPoints.Champions;
using RiotGamesApi.AspNetCore.RiotApi.NonStaticEndPoints.Match;
using System.Threading.Tasks;
using RiotGamesApi.AspNetCore.RiotApi.StaticEndPoints.Runes;
using RiotGamesApi.AspNetCore.RiotApi.StaticEndPoints.SummonerSpell;
using FindMyPlayer.Models.DTO;
using System.Linq;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers.Utils {
    public class LeagueOfLegendsAPI {

        public static string APIKEY ( ) {
            string result = "";
            try {
                using (var _context = new EntidadesContexto()) {
                    result = _context.DeveloperLolKeyu.First().Key;
                }
            } catch (Exception e ) { }
            return result;
        }

        public static async Task<RiotNet.Models.Summoner> RetornarLolUserAsync( string nick, ServicePlatform regiao ) {
            //Api Api = new Api( );
            //try {
            //    var summoner = await Api.NonStaticApi.Summonerv3.GetSummonersByNameAsync( regiao, nick );
            //    return summoner.Result;
            //} catch {
            //    return null;
            //}

            RiotNet.IRiotClient client = new RiotNet.RiotClient( new RiotNet.RiotClientSettings {
                ApiKey = LeagueOfLegendsAPI.APIKEY()
            } );

            RiotNet.Models.Summoner summoner = await client.GetSummonerBySummonerNameAsync( nick, RiotNet.Models.PlatformId.BR1 ).ConfigureAwait( false );
            return summoner;
        }

   

        public static async Task<SummonerSpellListDto> RetornarFeiticosAsync() {
            Api Api = new Api();
            var spells = await Api.StaticApi.StaticDatav3.GetSummonerSpellsAsync( platform: ServicePlatform.NA1, _useCache: true, _locale: null, _version: null, _tags: new List<SummonerSpellTag>() { SummonerSpellTag.image } );
            return spells.Result;
        }

        public static async Task<RuneListDto> RetornarRunasAsync() {
            Api Api = new Api();
            var runes = await Api.StaticApi.StaticDatav3.GetRunesAsync( platform: ServicePlatform.NA1, _useCache: true, _locale: null, _version: null, _tags: new List<RuneTag>() { RuneTag.image, RuneTag.stats } );
            return runes.Result;
        }

        public static async Task<ItemListDto> RetornarItensAsync(  ) {
            Api Api = new Api( );
            var itens = await Api.StaticApi.StaticDatav3.GetItemsAsync(platform: ServicePlatform.NA1, _useCache: true, _locale: null, _version: null, _tags: new List<ItemTag>( ) { ItemTag.image, ItemTag.stats, ItemTag.gold } );
            return itens.Result;
        }

        public static ChampionListDto RetornarHerois() {
            var Api = new ApiCall( )
                .SelectApi<ChampionListDto>( RiotGamesApi.AspNetCore.Enums.LolApiName.StaticData )
                .For( RiotGamesApi.AspNetCore.Enums.LolApiMethodName.Champions )
                .AddParameter( )
                .Build( ServicePlatform.NA1 )
                .UseCache( true )
                .Get( new QueryParameter("tags", ChampionTag.image ),
                new QueryParameter("tags", ChampionTag.lore));
            return Api.Result;
        }

        public static async Task<DadosPartida> RetornarPartidaAtualAsync(ServicePlatform regiao, int summonerId) {
            Api Api = new Api( );
            var dados = await Api.NonStaticApi.Spectatorv3.GetActiveGamesBySummonerAsync( regiao, summonerId );
            var match = await Api.NonStaticApi.Matchv3.GetMatchesOnlyMatchIdAsync( regiao, dados.Result.gameId );
            var partida = new DadosPartida {
                MapId = match.Result.mapId,
                GameType = dados.Result.gameType
            };
            Jogador j;
            foreach(var par in match.Result.participants) {
                j = new Jogador {
                    SummonerId = par.participantId,

                    ChampionId = par.championId,

                    Kills = par.stats.kills,
                    Deaths = par.stats.deaths,
                    Assistence = par.stats.assists,

                    Farm = par.stats.goldEarned
                };

                partida.Jogadores.Add( j );
            }
            return partida;
        }

        public static async Task<DadosNaoEstaticosUsuario> RetornarTaxasDoUsuarioAsync(ServicePlatform regiao, int accountId, int summonerId ) {
            Api api = new Api();
            DadosNaoEstaticosUsuario dados = new DadosNaoEstaticosUsuario();
            double vitorias = 0;
            double derrotas = 0;
            var recent = await api.NonStaticApi.Matchv3.GetMatchListsByAccountRecentAsync( regiao, accountId );
            if (recent.Result != null) {
                var roles = new List<string>();
                foreach (var x in recent.Result.matches) {

                    var match = await api.NonStaticApi.Matchv3.GetMatchesOnlyMatchIdAsync( regiao, x.gameId );
                    var participantId = match.Result.participantIdentities.SingleOrDefault( a => a.player.accountId == accountId );
                    var porcentagemVitorias = 0.0;

                    var participante = match.Result.participants.SingleOrDefault( a => a.participantId == participantId.participantId );
                    if (participante.stats.win) {
                        vitorias++;
                        porcentagemVitorias = 1;
                    } else {
                        derrotas++;
                    }

                    if (dados.DesempenhoClasse.TryGetValue( x.role, out double v )) {
                        porcentagemVitorias = porcentagemVitorias + v;
                        dados.DesempenhoClasse[ x.role ] = porcentagemVitorias;
                    } else {
                        dados.DesempenhoClasse.Add( x.role, porcentagemVitorias );
                        roles.Add( x.role );
                    }
                }
                dados.DesempenhoRecente = ( vitorias / 20 ) * 100;
                foreach (var d in roles) {
                    dados.DesempenhoClasse.TryGetValue( d, out double val );
                    dados.DesempenhoClasse[ d ] = val / vitorias * 100.0;
                }


                var positions = await api.NonStaticApi.Leaguev3.GetPositionsBySummonerAsync( regiao, summonerId );

                foreach (var h in positions.Result) {
                    dados.RankPositions.Add( new RankPositions {
                        Nome = h.leagueName,
                        Rank = h.rank,
                        Tier = h.tier,
                        Vitorias = h.wins,
                        Derrotas = h.losses
                    } );
                }

                using (var _context = new EntidadesContexto()) {
                    var lol_users = _context.LeagueOfLegends.OrderBy( a => a.NumeroVitorias ).ToList();
                    var lol = lol_users.SingleOrDefault( a => a.SummonerId == summonerId );
                    dados.RankInterno = lol_users.IndexOf( lol ) + 1;
                }
            } 

            return dados;
        }

        public static async Task<int> RecuperarVitoriasAsync(ServicePlatform regiao, int accountId, int summonerId) {
            Api api = new Api();
            var recent = await api.NonStaticApi.Matchv3.GetMatchListsByAccountRecentAsync( regiao, accountId );
            var vitorias = 0;
            foreach (var x in recent.Result.matches) {
                var match = await api.NonStaticApi.Matchv3.GetMatchesOnlyMatchIdAsync( regiao, x.gameId );
                var participantId = match.Result.participantIdentities.SingleOrDefault( a => a.player.accountId == accountId );

                var participante = match.Result.participants.SingleOrDefault( a => a.participantId == participantId.participantId );
                if (participante.stats.win) {
                    vitorias++;
                }
            }
            return vitorias;
        }

        public static ServicePlatform RetornarRegiao( int regiaoId ) {
            switch (regiaoId) {
                case 1:
                    return ServicePlatform.BR1;
                case 2:
                    return ServicePlatform.EUN1;
                case 3:
                    return ServicePlatform.EUW1;
                case 4:
                    return ServicePlatform.JP1;
                case 5:
                    return ServicePlatform.KR;
                case 6:
                    return ServicePlatform.LA1;
                case 7:
                    return ServicePlatform.LA2;
                case 8:
                    return ServicePlatform.NA1;
                case 9:
                    return ServicePlatform.OC1;
                case 10:
                    return ServicePlatform.PBE1;
                case 11:
                    return ServicePlatform.RU;
                case 12:
                    return ServicePlatform.TR1;
                default:
                    return ServicePlatform.UNDEFINED;
            }
        }
    }
}
