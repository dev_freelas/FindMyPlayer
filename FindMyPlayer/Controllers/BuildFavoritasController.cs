﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/BuildFavoritas" )]
    public class BuildFavoritasController : Controller {
        private readonly EntidadesContexto _context;

        public BuildFavoritasController() {
            _context = new EntidadesContexto();
        }

        // GET: api/BuildFavoritas
        [HttpGet]
        public IEnumerable<BuildFavorita> GetBuildFavoritas() {
            return _context.BuildFavoritas;
        }

        // GET: api/BuildFavoritas/5
        [HttpGet( "{id}" )]
        public IActionResult GetBuildFavorita([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildFavorita = _context.BuildFavoritas.Where( m => m.UsuarioId == id ).ToList();

            if (buildFavorita == null) {
                return NotFound();
            }

            return Ok( buildFavorita );
        }

        // POST: api/BuildFavoritas
        [HttpPost]
        public async Task<IActionResult> PostBuildFavorita([FromBody] BuildFavorita buildFavorita) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.BuildFavoritas.Add( buildFavorita );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetBuildFavorita", new { id = buildFavorita.BuildFavoritaId }, buildFavorita );
        }

        // DELETE: api/BuildFavoritas/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteBuildFavorita([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildFavorita = await _context.BuildFavoritas.SingleOrDefaultAsync( m => m.BuildFavoritaId == id );
            if (buildFavorita == null) {
                return NotFound();
            }

            _context.BuildFavoritas.Remove( buildFavorita );
            await _context.SaveChangesAsync();

            return Ok( buildFavorita );
        }

        private bool BuildFavoritaExists(int id) {
            return _context.BuildFavoritas.Any( e => e.BuildFavoritaId == id );
        }
    }
}