﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/ChatGrupos" )]
    public class ChatGruposController : Controller {
        private readonly EntidadesContexto _context;

        public ChatGruposController( ) {
            _context = new EntidadesContexto();
        }

        // GET: api/ChatGrupoes
        [HttpGet]
        public IEnumerable<ChatGrupo> GetChatGrupo() {
            return _context.ChatGrupo;
        }

        // GET: api/ChatGrupoes/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetChatGrupo([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatGrupo = await _context.ChatGrupo.SingleOrDefaultAsync( m => m.ChatGrupoId == id );

            if (chatGrupo == null) {
                return NotFound();
            }

            return Ok( chatGrupo );
        }

        [HttpGet( "GetChatGruposUsuario/{id}&{tipo}" )]
        [Route("GetChatGruposUsuario/")]
        public async Task<IActionResult> GetChatGruposUsuario([FromRoute] int id, [FromRoute]int tipo) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }
            List<ChatGrupo> list = new List<ChatGrupo>();
            var d = _context.ChatGrupoUsuario.Where( a => a.UsuarioId == id ).ToList();
            foreach(var a in d) {
                list.Add( await _context.ChatGrupo.SingleOrDefaultAsync( e => e.ChatGrupoId == a.ChatGrupoId ) );
            }

            if (list.Count() == 0) {
                return NotFound();
            }

            if (tipo == 1) {
                return Ok( list.Where( a => a.Privado == true ) );
            } else if (tipo == 2) {
                return Ok( list.Where( a => a.Privado == false ) );
            }

            return Ok( list );
        }

        // PUT: api/ChatGrupoes/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutChatGrupo([FromRoute] int id, [FromBody] ChatGrupo chatGrupo) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != chatGrupo.ChatGrupoId) {
                return BadRequest();
            }

            _context.Entry( chatGrupo ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!ChatGrupoExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ChatGrupoes
        [HttpPost("{usuarioId}")]
        public async Task<IActionResult> PostChatGrupo([FromBody] ChatGrupo chatGrupo, [FromRoute] int usuarioId) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.ChatGrupo.Add( chatGrupo );
            await _context.SaveChangesAsync();
            _context.ChatGrupoUsuario.Add( new ChatGrupoUsuario {
                UsuarioId = usuarioId,
                ChatGrupoId = chatGrupo.ChatGrupoId
            } );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetChatGrupo", new { id = chatGrupo.ChatGrupoId }, chatGrupo );
        }

        // DELETE: api/ChatGrupoes/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteChatGrupo([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatGrupo = await _context.ChatGrupo.SingleOrDefaultAsync( m => m.ChatGrupoId == id );
            if (chatGrupo == null) {
                return NotFound();
            }

            _context.ChatGrupo.Remove( chatGrupo );
            await _context.SaveChangesAsync();

            return Ok( chatGrupo );
        }

        [HttpPost("InserirMembro/{chatGrupoUsuario}")]
        [Route("InserirMembro")]
        public async Task<IActionResult> InserirMembro([FromBody] ChatGrupoUsuario chatGrupoUsuario) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var usuario = await _context.Usuarios.SingleOrDefaultAsync( m => m.UsuarioId == chatGrupoUsuario.UsuarioId );
            if (usuario == null) {
                return NotFound();
            }

            var chatGrupo = await _context.ChatGrupo.SingleOrDefaultAsync( m => m.ChatGrupoId == chatGrupoUsuario.ChatGrupoId );
            if (chatGrupo == null) {
                return NotFound();
            }

            var validar = await _context.ChatGrupoUsuario.SingleOrDefaultAsync( m => m.ChatGrupoId == chatGrupo.ChatGrupoId && m.UsuarioId == usuario.UsuarioId );
            if(validar != null) {
                return BadRequest( new Error { Codigo = 400, Mensagem = "Usuario ja encontra-se nesse grupo" } );
            }

            _context.ChatGrupoUsuario.Add( chatGrupoUsuario );
            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpDelete("RemoverMembro/{usuarioId}&{grupoId}")]
        [Route("RemoverMembro")]
        public async Task<IActionResult> RemoverMembro ( [FromRoute] int usuarioId, [FromRoute] int grupoId) {

            var grupo = await _context.ChatGrupo.SingleOrDefaultAsync( m => m.ChatGrupoId == grupoId );
            if(grupo == null) {
                return NotFound();
            }

            var usuarioGrupo = await _context.ChatGrupoUsuario.SingleOrDefaultAsync( m => m.ChatGrupoId == grupoId && m.UsuarioId == usuarioId );
            if( usuarioGrupo == null) {
                return NotFound( new Error { Codigo = 404, Mensagem = "Usuario nao esta no grupo" } );
            }

            _context.ChatGrupoUsuario.Remove( usuarioGrupo );

            await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpGet("ListarMembros/{grupoId}")]
        [Route("ListarMembros/")]
        public async Task<IActionResult> ListarMembros( int grupoId) {
            var grupo = await _context.ChatGrupo.SingleOrDefaultAsync( m => m.ChatGrupoId == grupoId );
            if (grupo == null) {
                return NotFound();
            }

            var resp = await _context.ChatGrupoUsuario.Where( a => a.ChatGrupoId == grupoId ).Select( m => m.UsuarioId).ToListAsync();

            return Ok( resp );
        }

        private bool ChatGrupoExists(int id) {
            return _context.ChatGrupo.Any( e => e.ChatGrupoId == id );
        }
    }
}