using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;
using System.Diagnostics;
using FindMyPlayer.Controllers.Utils;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/LeagueOfLegends" )]
    public class LeagueOfLegendsController : Controller {
        private readonly EntidadesContexto _context;

        public LeagueOfLegendsController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/LeagueOfLegends
        [HttpGet]
        public IEnumerable<LeagueOfLegends> GetLeagueOfLegends() {
            return _context.LeagueOfLegends;
        }

        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetLeagueOfLegends( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 200, Mensagem = "Informacoes inconsistentes", Status = "Erro" } );
            }

            var leagueOfLegends = await _context.LeagueOfLegends.SingleOrDefaultAsync( m => m.LeagueOfLegendsId == id );

            if (leagueOfLegends == null) {
                return new ObjectResult( new Error { Codigo = 201, Mensagem = "Esse id nao e valido.", Status = "Erro" } );
            }

            return Ok( leagueOfLegends );
        }

        [HttpPut( "Editar/{id}" )]
        [Route("Editar")]
        public async Task<IActionResult> EditarLeagueOfLegends( [FromRoute] int id, [FromBody] LeagueOfLegends leagueOfLegends ) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 200, Mensagem = "Informacoes inconsistentes", Status = "Erro" } );
            }

            if (id != leagueOfLegends.LeagueOfLegendsId) {
                return new ObjectResult( new Error { Codigo = 201, Mensagem = "Esse Id nao pertence a essa conta", Status = "Erro" } );
            }

            _context.Entry( leagueOfLegends ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!LeagueOfLegendsExists( id )) {
                    return new ObjectResult( new Error { Codigo = 210, Mensagem = "Problema ao salvar, tente novamente.", Status = "Erro" } );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        [HttpPost("Cadastrar/{leagueOfLegends}")]
        [Route("Cadastrar")]
        public async Task<IActionResult> PostLeagueOfLegends( [FromBody] LeagueOfLegends leagueOfLegends ) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 200, Mensagem = "Informacoes inconsistentes.", Status = "Erro" } );
            }

            // Valida o nick na base do League of Legends
            var lol = await LeagueOfLegendsAPI.RetornarLolUserAsync( leagueOfLegends.Nick, LeagueOfLegendsAPI.RetornarRegiao( leagueOfLegends.RegiaoId ) );
            if (lol == null || lol.Id == 0) {
                return new ObjectResult( new Error { Codigo = 202, Mensagem = "Esse nick nao e valido.", Status = "Erro" } );
            }
            leagueOfLegends.SummonerId = (int) lol.Id;
            leagueOfLegends.AccountId = (int) lol.AccountId;
            leagueOfLegends.Level = Convert.ToInt32(lol.SummonerLevel);
            leagueOfLegends.Imagem = lol.ProfileIconId.ToString()+".png";

            // Valida se usuario j� cadastrado
            if (_context.LeagueOfLegends.SingleOrDefault( a => a.SummonerId == leagueOfLegends.SummonerId ) != null) {
                return new ObjectResult( new Error { Codigo = 203, Mensagem = "Esse summonerId ja esta vinculado a um usuario.", Status = "Erro" } );
            }

            _context.LeagueOfLegends.Add( leagueOfLegends );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetLeagueOfLegends", new { id = leagueOfLegends.LeagueOfLegendsId }, leagueOfLegends );
        }

        // ESPECTADOR: api/LeagueOfLegends/espectador/5
        [HttpGet( "Espectador/{id}" )]
        [Route( "LeagueOfLegends/Espectador" )]
        public async Task<IActionResult> EspectadorLeagueOfLegendsAsync( [FromRoute] int id ) {
            var leagueOfLegends = _context.LeagueOfLegends.SingleOrDefault( a => a.LeagueOfLegendsId == id );
            if( leagueOfLegends == null) {
                return new ObjectResult( new Error( ) { Codigo = 201, Mensagem = "Usuario nao existe" } );
            }

            var lol = await LeagueOfLegendsAPI.RetornarPartidaAtualAsync( LeagueOfLegendsAPI.RetornarRegiao( leagueOfLegends.RegiaoId ), leagueOfLegends.SummonerId );

            if (lol == null) {
                return new ObjectResult( new Error( ) { Codigo = 205, Mensagem = "Usuario nao esta em uma partida" } );
            }

            return Ok( lol );
        }

        [HttpGet( "GetTaxasUsuario/{id}" )]
        [Route( "LeagueOfLegends/GetTaxasUsuario" )]
        public async Task<IActionResult> GetTaxasUsuarioAsync( [FromRoute] int id) {
            var lol = _context.LeagueOfLegends.SingleOrDefault( a => a.LeagueOfLegendsId == id);
            if (lol == null) {
                BadRequest( new Error() { Codigo = 404, Mensagem = "Usuario n�o encontrado" } );
            }

            var dados = await LeagueOfLegendsAPI.RetornarTaxasDoUsuarioAsync( RiotGamesApi.AspNetCore.RiotApi.Enums.ServicePlatform.BR1, lol.AccountId, lol.SummonerId );
            return Ok( dados );

        }
        
        private bool LeagueOfLegendsExists( int id ) {
            return _context.LeagueOfLegends.Any( e => e.LeagueOfLegendsId == id );
        }
    }
}