using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Builds" )]
    public class BuildsController : Controller {
        private readonly EntidadesContexto _context;

        public BuildsController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Builds
        [HttpGet]
        public IEnumerable<Build> GetBuilds() {
            return _context.Builds;
        }

        // GET: api/Builds/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetBuild( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var build = await _context.Builds.SingleOrDefaultAsync( m => m.BuildId == id );

            if (build == null) {
                return NotFound( );
            }

            return Ok( build );
        }

        [HttpGet( "BuildsHeroi/{heroiId}" )]
        [Route( "BuildsHeroi" )]
        public IActionResult BuildsHeroi([FromRoute] int heroiId) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var builds = _context.Builds.Where( m => m.HeroiId == heroiId ).ToList();

            if (builds == null) {
                return NotFound();
            }

            return Ok( builds );
        }
        // PUT: api/Builds/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutBuild( [FromRoute] int id, [FromBody] Build build ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != build.BuildId) {
                return BadRequest( );
            }

            _context.Entry( build ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!BuildExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Builds
        [HttpPost]
        public async Task<IActionResult> PostBuild( [FromBody] Build build ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Builds.Add( build );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetBuild", new { id = build.BuildId }, build );
        }

        // DELETE: api/Builds/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteBuild( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var build = await _context.Builds.SingleOrDefaultAsync( m => m.BuildId == id );
            if (build == null) {
                return NotFound( );
            }

            _context.Builds.Remove( build );
            await _context.SaveChangesAsync( );

            return Ok( build );
        }

        private bool BuildExists( int id ) {
            return _context.Builds.Any( e => e.BuildId == id );
        }
    }
}