﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Feiticos" )]
    public class FeiticosController : Controller {
        private readonly EntidadesContexto _context;

        public FeiticosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Feiticos
        [HttpGet]
        public IEnumerable<Feitico> GetFeiticos() {
            return _context.Feiticos;
        }

        // GET: api/Feiticos/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetFeitico([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var feitico = await _context.Feiticos.SingleOrDefaultAsync( m => m.FeiticoId == id );

            if (feitico == null) {
                return NotFound();
            }

            return Ok( feitico );
        }

        // PUT: api/Feiticos/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutFeitico([FromRoute] int id, [FromBody] Feitico feitico) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != feitico.FeiticoId) {
                return BadRequest();
            }

            _context.Entry( feitico ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!FeiticoExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Feiticos
        [HttpPost]
        public async Task<IActionResult> PostFeitico([FromBody] Feitico feitico) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Feiticos.Add( feitico );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetFeitico", new { id = feitico.FeiticoId }, feitico );
        }

        // DELETE: api/Feiticos/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteFeitico([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var feitico = await _context.Feiticos.SingleOrDefaultAsync( m => m.FeiticoId == id );
            if (feitico == null) {
                return NotFound();
            }

            _context.Feiticos.Remove( feitico );
            await _context.SaveChangesAsync();

            return Ok( feitico );
        }

        private bool FeiticoExists(int id) {
            return _context.Feiticos.Any( e => e.FeiticoId == id );
        }
    }
}