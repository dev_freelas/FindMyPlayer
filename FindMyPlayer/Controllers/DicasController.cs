using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Dicas" )]
    public class DicasController : Controller {
        private readonly EntidadesContexto _context;

        public DicasController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Dicas
        [HttpGet]
        public IEnumerable<Dica> GetDicas() {
            return _context.Dicas;
        }

        // GET: api/Dicas/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetDica( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var dica = await _context.Dicas.SingleOrDefaultAsync( m => m.DicaId == id );

            if (dica == null) {
                return NotFound( );
            }

            return Ok( dica );
        }

        // PUT: api/Dicas/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutDica( [FromRoute] int id, [FromBody] Dica dica ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != dica.DicaId) {
                return BadRequest( );
            }

            _context.Entry( dica ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!DicaExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Dicas
        [HttpPost]
        public async Task<IActionResult> PostDica( [FromBody] Dica dica ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Dicas.Add( dica );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetDica", new { id = dica.DicaId }, dica );
        }

        // DELETE: api/Dicas/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteDica( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var dica = await _context.Dicas.SingleOrDefaultAsync( m => m.DicaId == id );
            if (dica == null) {
                return NotFound( );
            }

            _context.Dicas.Remove( dica );
            await _context.SaveChangesAsync( );

            return Ok( dica );
        }

        private bool DicaExists( int id ) {
            return _context.Dicas.Any( e => e.DicaId == id );
        }
    }
}