﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/AvaliacaoUsuarios" )]
    public class AvaliacaoUsuariosController : Controller {
        private readonly EntidadesContexto _context;

        public AvaliacaoUsuariosController() {
            _context = new EntidadesContexto();
        }

        // GET: api/AvaliacaoUsuarios
        [HttpGet]
        public IEnumerable<AvaliacaoUsuario> GetAvaliacaoUsuario() {
            return _context.AvaliacaoUsuario;
        }

        // GET: api/AvaliacaoUsuarios/5
        [HttpGet( "{id}" )]
        public IActionResult GetAvaliacaoUsuario([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var avaliacaoUsuario =  _context.AvaliacaoUsuario.Where( m => m.UsuarioAvaliadoId == id ).ToList();

            if (avaliacaoUsuario == null) {
                return NotFound();
            }

            return Ok( avaliacaoUsuario );
        }

        // PUT: api/AvaliacaoUsuarios/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutAvaliacaoUsuario([FromRoute] int id, [FromBody] AvaliacaoUsuario avaliacaoUsuario) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != avaliacaoUsuario.AvaliacaoUsuarioId) {
                return BadRequest();
            }

            _context.Entry( avaliacaoUsuario ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!AvaliacaoUsuarioExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/AvaliacaoUsuarios
        [HttpPost]
        public async Task<IActionResult> PostAvaliacaoUsuario([FromBody] AvaliacaoUsuario avaliacaoUsuario) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.AvaliacaoUsuario.Add( avaliacaoUsuario );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetAvaliacaoUsuario", new { id = avaliacaoUsuario.AvaliacaoUsuarioId }, avaliacaoUsuario );
        }

        // DELETE: api/AvaliacaoUsuarios/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteAvaliacaoUsuario([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var avaliacaoUsuario = await _context.AvaliacaoUsuario.SingleOrDefaultAsync( m => m.AvaliacaoUsuarioId == id );
            if (avaliacaoUsuario == null) {
                return NotFound();
            }

            _context.AvaliacaoUsuario.Remove( avaliacaoUsuario );
            await _context.SaveChangesAsync();

            return Ok( avaliacaoUsuario );
        }

        private bool AvaliacaoUsuarioExists(int id) {
            return _context.AvaliacaoUsuario.Any( e => e.AvaliacaoUsuarioId == id );
        }
    }
}