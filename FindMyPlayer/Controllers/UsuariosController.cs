using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Usuarios" )]
    public class UsuariosController : Controller {
        private readonly EntidadesContexto _context;

        public UsuariosController() {
            _context = new EntidadesContexto();
        }

        // GET: api/Usuarios
        [ HttpGet ]
        public IEnumerable<Usuario> GetUsuarios() {
            return _context.Usuarios;
        }

        // GET: api/Usuarios/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetUsuario([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 100, Mensagem = "Informacoes inconsistentes.", Status = "Erro" } );
            }

            var usuario = await _context.Usuarios.SingleOrDefaultAsync( m => m.UsuarioId == id );

            if (usuario == null) {
                return new ObjectResult( new Error { Codigo = 101, Mensagem = "Esse Id nao e valido.", Status = "Erro" } );
            }

            return Ok( usuario );
        }


        [HttpPut( "Editar/{id, usuario}" )]
        [Route( "Editar" )]
        public async Task<IActionResult> EditarUsuario([FromRoute] int id, [FromBody] Usuario usuario) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 101, Mensagem = "Informacoes inconsistentes.", Status = "Erro" } );
            }

            if (!Base.ValidarChave( id, usuario.ChaveSessao, usuario.TimeStamp )) {
                return new ObjectResult( new Error { Codigo = 100, Mensagem = "Chave invalida ou vencida." } );
            }


            if (id != usuario.UsuarioId) {
                return new ObjectResult( new Error { Codigo = 101, Mensagem = "Esse Id nao condiz com as informacoes desse usuario.", Status = "Erro" } );
            }

            _context.Entry( usuario ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!UsuarioExists( id )) {
                    return new ObjectResult( new Error { Codigo = 110, Mensagem = "Problema ao salvar, tente novamente.", Status = "Erro" } );
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost( "Cadastrar/{usuario}" )]
        [Route( "Cadastrar" )]
        public async Task<IActionResult> CadastrarUsuario([FromBody] Usuario usuario) {
            if (!ModelState.IsValid) {
                return new ObjectResult( new Error { Codigo = 100, Mensagem = "Informacoes inconsistentes.", Status = "Erro" } );
            }

            if (_context.Usuarios.SingleOrDefault( a => a.Email == usuario.Email ) != null) {
                return new ObjectResult( new Error() { Codigo = 101, Mensagem = "Esse email ja existe", Status = "Erro" } );
            }

            _context.Usuarios.Add( usuario );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetUsuario", new { id = usuario.UsuarioId }, usuario );
        }

        private bool UsuarioExists(int id) {
            return _context.Usuarios.Any( e => e.UsuarioId == id );
        }

        [HttpPost( "AlterarPagamento/{usuario}" )]
        [Route( "AlterarPagamento" )]
        public IActionResult AlterarPagamento([FromBody] Usuario usuario) {

            if (!Base.ValidarChave( usuario.UsuarioId, usuario.ChaveSessao, usuario.TimeStamp )) {
                return new ObjectResult( new Error { Codigo = 100, Mensagem = "Chave invalida ou vencida." } );
            }

            var user = _context.Usuarios.SingleOrDefault( a => a.UsuarioId == usuario.UsuarioId );

            if (user == null) {
                return new ObjectResult( new Error { Codigo = 404, Mensagem = "Usuario n�o existe" } );
            }

            if(usuario.DataCompra == null) {
                return new ObjectResult( new Error { Codigo = 404, Mensagem = "Data de compra invalida. Deve ser diferente de null" });
            }

            user.TipoUsuario = TipoUsuario.PREMIUM;
            user.DataCompra = usuario.DataCompra;
            _context.SaveChanges();
            return Ok( user );
        }

        [HttpGet("GetUsuarioPorEmail/{email}")]
        [Route("GetUsuarioPorEmail/")]
        public async Task<IActionResult> GetUSuarioPorEmail([FromRoute] string email) {
            var resp = await _context.Usuarios.Where( a => a.Email.Equals( email ) ).ToListAsync();
            return Ok( resp );
        }

        [HttpGet( "GetUsuarioPorNome/{nome}" )]
        [Route( "GetUsuarioPorNome/" )]
        public async Task<IActionResult> GetUSuarioPorNome([FromRoute] string nome) {
            var resp = await _context.Usuarios.Where( a => a.Nome.Equals( nome ) ).ToListAsync();
            return Ok( resp );
        }

        [HttpPost( "Login/{usuario}" )]
        [Route( "Login" )]
        public IActionResult Login([FromBody] Usuario usuario) {

            var user = _context.Usuarios.SingleOrDefault( a => a.Email.Equals( usuario.Email ) );
            if (user != null) {
                if (user.SenhaHash == usuario.SenhaHash) {
                    var sessao = _context.SessasoUsuario.SingleOrDefault( a => a.UsuarioId == user.UsuarioId );
                    var time = DateTime.Now;
                    if (sessao != null) {
                        sessao.ChaveSessao = GenerateKey( usuario.Email + time );
                        sessao.TimeStamp = time;
                        user.ChaveSessao = sessao.ChaveSessao;
                    } else {
                        var userSessao = new SessaoUsuario {
                            UsuarioId = user.UsuarioId,
                            TimeStamp = time,
                            ChaveSessao = GenerateKey( usuario.Email + time.ToString() )
                        };
                        _context.SessasoUsuario.Add( userSessao );
                        user.ChaveSessao = userSessao.ChaveSessao;
                        user.TimeStamp = time;
                    }
                    _context.SaveChanges();
                    return Ok( user );
                } else {
                    return new ObjectResult( new Error { Codigo = 102, Mensagem = "Esse senha nao e valida para esse usuario.", Status = "Erro" } );
                }
            } else {
                return new ObjectResult( new Error { Codigo = 101, Mensagem = "Esse usuario nao existe.", Status = "Erro" } );
            }
        }

        public string GenerateKey(string data) {
            byte[] hash;
            string f = "";
            using (MD5 md5 = MD5.Create()) {
                md5.Initialize();
                md5.ComputeHash( Encoding.UTF8.GetBytes( data ) );
                hash = md5.Hash;
                f = md5.Hash.ToString();
            }
            return ToHex( hash, false );
        }

        public static string ToHex(byte[] bytes, bool upperCase) {
            StringBuilder result = new StringBuilder( bytes.Length * 2 );

            for (int i = 0; i < bytes.Length; i++)
                result.Append( bytes[ i ].ToString( upperCase ? "X2" : "x2" ) );

            return result.ToString();
        }
    }
}