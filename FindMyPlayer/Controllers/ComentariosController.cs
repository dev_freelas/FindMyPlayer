using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Comentarios" )]
    public class ComentariosController : Controller {
        private readonly EntidadesContexto _context;

        public ComentariosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Comentarios
        [HttpGet]
        public IEnumerable<Comentario> GetComentarios() {
            return _context.Comentarios;
        }

        // GET: api/Comentarios/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetComentario( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var comentario = await _context.Comentarios.SingleOrDefaultAsync( m => m.ComentarioId == id );

            if (comentario == null) {
                return NotFound( );
            }

            return Ok( comentario );
        }

        // PUT: api/Comentarios/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutComentario( [FromRoute] int id, [FromBody] Comentario comentario ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != comentario.ComentarioId) {
                return BadRequest( );
            }

            _context.Entry( comentario ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!ComentarioExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Comentarios
        [HttpPost]
        public async Task<IActionResult> PostComentario( [FromBody] Comentario comentario ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Comentarios.Add( comentario );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetComentario", new { id = comentario.ComentarioId }, comentario );
        }

        // DELETE: api/Comentarios/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteComentario( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var comentario = await _context.Comentarios.SingleOrDefaultAsync( m => m.ComentarioId == id );
            if (comentario == null) {
                return NotFound( );
            }

            _context.Comentarios.Remove( comentario );
            await _context.SaveChangesAsync( );

            return Ok( comentario );
        }

        private bool ComentarioExists( int id ) {
            return _context.Comentarios.Any( e => e.ComentarioId == id );
        }
    }
}