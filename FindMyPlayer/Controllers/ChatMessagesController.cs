﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;
using FindMyPlayer.Controllers.Utils;
using FindMyPlayer.Controllers.Utils.WebSocketManager;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/ChatMessages" )]
    public class ChatMessagesController : Controller {
        private readonly EntidadesContexto _context;

        private NotificationsMessageHandler _notificationsMessageHandler { get; set; }

        public ChatMessagesController(NotificationsMessageHandler notificationsMessageHandler) {
            _context = new EntidadesContexto();
            _notificationsMessageHandler = notificationsMessageHandler;
        }

        // GET: api/ChatMessages
        [HttpGet]
        public IEnumerable<ChatMessages> GetChatMessages() {
            return _context.ChatMessages;
        }

        // GET: api/ChatMessages/5
        [HttpGet( "GetAllMessagesReceiveByUser/{id}" )]
        [Route( "GetAllMessagesReceiveByUser" )]
        public IActionResult GetAllMessagesReceiveByUser([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatMessages = _context.ChatMessages.Where( m => m.Usuario2Id == id && m.ChatStatusId == 1 ).ToList();

            if (chatMessages == null || chatMessages.Count() == 0) {
                return NotFound();
            }

            var groupMessages = chatMessages.GroupBy( a => a.Usuario2Id ).ToList();
            for (int i = 0; i < chatMessages.Count(); i++) {
                chatMessages.ElementAt( 0 ).ChatStatusId = 2;
            }
            _context.SaveChanges();

            return Ok( groupMessages );
        }

        [HttpGet( "GetAllMessagesSendByUser/{id}" )]
        [Route( "GetAllMessagesSendByUser" )]
        public IActionResult GetAllMessagesSendByUser([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatMessages = _context.ChatMessages.Where( m => m.Usuario1Id == id ).ToList();

            if (chatMessages == null) {
                return NotFound();
            }

            var groupMessages = chatMessages.GroupBy( a => a.Usuario2Id ).ToList();

            for (int i = 0; i < groupMessages.Count(); i++) {
                foreach (var f in groupMessages.ElementAt( i )) {
                    f.ChatStatusId = 2;
                }
            }

            _context.SaveChanges();

            return Ok( groupMessages );
        }

        [HttpGet( "GetMessagesFromTo/{idFrom}&{idTo}" )]
        [Route( "GetMessagesFromTo" )]
        public IActionResult GetMessagesFromTo([FromRoute] int idFrom, [FromRoute] int idTo) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatMessages = _context.ChatMessages.Where( m => m.Usuario1Id == idFrom && m.Usuario2Id == idTo && m.ChatStatusId == 1 ).ToList();

            if (chatMessages == null) {
                return NotFound();
            }

            var groupMessages = chatMessages.GroupBy( a => a.Usuario2Id ).ToList();

            for (int i = 0; i < groupMessages.Count(); i++) {
                foreach (var f in groupMessages.ElementAt( i )) {
                    f.ChatStatusId = 2;
                }
            }

            _context.SaveChanges();

            return Ok( groupMessages );
        }

        [HttpGet( "GetMessagesGroupo/{groupoId}" )]
        [Route( "GetMessagesGroupo" )]
        public IActionResult GetMessagesGroup([FromRoute] int groupoId) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatMessages = _context.ChatMessages.Where( m => m.ChatGrupoId == groupoId && m.ChatStatusId == 1 ).ToList();

            if (chatMessages == null) {
                return NotFound();
            }

            foreach (var f in chatMessages) {
                f.ChatStatusId = 2;
            }

            _context.SaveChanges();

            return Ok( chatMessages );
        }

        // POST: api/ChatMessages
        [HttpPost( "SendMessage/{chatMessages}" )]
        [Route( "SendMessage" )]
        public async Task<IActionResult> SendMessage([FromBody] ChatMessages chatMessages) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (chatMessages.Usuario2Id == null && chatMessages.ChatGrupoId == null) {
                return BadRequest( new Error { Codigo = 400, Mensagem = "Mensagem nao contem destinatario" } );
            }

            chatMessages.ChatStatusId = 1;
            _context.ChatMessages.Add( chatMessages );
            await _context.SaveChangesAsync();

            var ggg = Caminhos.usuariosConectados;
            var userId = "";
            if (ggg.Count() > 0) {
                userId = Caminhos.usuariosConectados.SingleOrDefault( a => Convert.ToInt32( a ) == chatMessages.Usuario2Id );
            }
            if (chatMessages.Usuario2Id != null) {
                if (!String.IsNullOrEmpty( userId )) {
                    await _notificationsMessageHandler.SendMessageAsync( userId, "new_message:u:"+chatMessages.Usuario1Id );
                    return Ok( new Error { Codigo = 200, Mensagem = "Mensagem enviada e usuario foi notificado" } );
                } else {
                    return Ok( new Error { Codigo = 201, Mensagem = "Mensagem enviada mas usuario offline" } );
                }
            } else {
                var users = _context.ChatGrupoUsuario.Where( a => a.ChatGrupoId == chatMessages.ChatGrupoId ).ToList();
                foreach (var user in users) {
                    userId = Caminhos.usuariosConectados.SingleOrDefault( a => Convert.ToInt32( a ) == user.UsuarioId );
                    if (!String.IsNullOrEmpty( userId )) {
                        await _notificationsMessageHandler.SendMessageAsync( userId, "new_message:g:"+chatMessages.ChatGrupoId );
                    }
                }
                return Ok( new Error { Codigo = 200, Mensagem = "Mensagens enviadas e os usuarios onlines foram notificados" } );
            }
        }

        // DELETE: api/ChatMessages/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteChatMessages([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var chatMessages = await _context.ChatMessages.SingleOrDefaultAsync( m => m.ChatId == id );
            if (chatMessages == null) {
                return NotFound();
            }

            _context.ChatMessages.Remove( chatMessages );
            await _context.SaveChangesAsync();

            return Ok( chatMessages );
        }

        private bool ChatMessagesExists(int id) {
            return _context.ChatMessages.Any( e => e.ChatId == id );
        }

    }
}