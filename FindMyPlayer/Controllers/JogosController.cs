using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Jogos" )]
    public class JogosController : Controller {
        private readonly EntidadesContexto _context;

        public JogosController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Jogos
        [HttpGet]
        public IEnumerable<Jogo> GetJogos() {
            return _context.Jogos;
        }

        // GET: api/Jogos/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetJogo( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var jogo = await _context.Jogos.SingleOrDefaultAsync( m => m.JogoId == id );

            if (jogo == null) {
                return NotFound( );
            }

            return Ok( jogo );
        }

        // PUT: api/Jogos/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutJogo( [FromRoute] int id, [FromBody] Jogo jogo ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != jogo.JogoId) {
                return BadRequest( );
            }

            _context.Entry( jogo ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!JogoExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Jogos
        [HttpPost]
        public async Task<IActionResult> PostJogo( [FromBody] Jogo jogo ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Jogos.Add( jogo );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetJogo", new { id = jogo.JogoId }, jogo );
        }

        // DELETE: api/Jogos/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteJogo( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var jogo = await _context.Jogos.SingleOrDefaultAsync( m => m.JogoId == id );
            if (jogo == null) {
                return NotFound( );
            }

            _context.Jogos.Remove( jogo );
            await _context.SaveChangesAsync( );

            return Ok( jogo );
        }

        private bool JogoExists( int id ) {
            return _context.Jogos.Any( e => e.JogoId == id );
        }
    }
}