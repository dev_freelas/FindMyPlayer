﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/BuildRunas" )]
    public class BuildRunasController : Controller {
        private readonly EntidadesContexto _context;

        public BuildRunasController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/BuildRunas
        [HttpGet]
        public IEnumerable<BuildRuna> GetBuildRunas() {
            return _context.BuildRunas;
        }

        // GET: api/BuildRunas/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetBuildRuna([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildRuna = await _context.BuildRunas.SingleOrDefaultAsync( m => m.BuildRunaId == id );

            if (buildRuna == null) {
                return NotFound();
            }

            return Ok( buildRuna );
        }

        // PUT: api/BuildRunas/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutBuildRuna([FromRoute] int id, [FromBody] BuildRuna buildRuna) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != buildRuna.BuildRunaId) {
                return BadRequest();
            }

            _context.Entry( buildRuna ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
                if (!BuildRunaExists( id )) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/BuildRunas
        [HttpPost]
        public async Task<IActionResult> PostBuildRuna([FromBody] BuildRuna buildRuna) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.BuildRunas.Add( buildRuna );
            await _context.SaveChangesAsync();

            return CreatedAtAction( "GetBuildRuna", new { id = buildRuna.BuildRunaId }, buildRuna );
        }

        // DELETE: api/BuildRunas/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteBuildRuna([FromRoute] int id) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildRuna = await _context.BuildRunas.SingleOrDefaultAsync( m => m.BuildRunaId == id );
            if (buildRuna == null) {
                return NotFound();
            }

            _context.BuildRunas.Remove( buildRuna );
            await _context.SaveChangesAsync();

            return Ok( buildRuna );
        }

        private bool BuildRunaExists(int id) {
            return _context.BuildRunas.Any( e => e.BuildRunaId == id );
        }
    }
}