using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Lanes" )]
    public class LanesController : Controller {
        private readonly EntidadesContexto _context;

        public LanesController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Lanes
        [HttpGet]
        public IEnumerable<Lane> GetLane() {
            return _context.Lanes;
        }

        // GET: api/Lanes/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetLane( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var lane = await _context.Lanes.SingleOrDefaultAsync( m => m.LaneId == id );

            if (lane == null) {
                return NotFound( );
            }

            return Ok( lane );
        }

        // PUT: api/Lanes/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutLane( [FromRoute] int id, [FromBody] Lane lane ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != lane.LaneId) {
                return BadRequest( );
            }

            _context.Entry( lane ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!LaneExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Lanes
        [HttpPost]
        public async Task<IActionResult> PostLane( [FromBody] Lane lane ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Lanes.Add( lane );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetLane", new { id = lane.LaneId }, lane );
        }

        // DELETE: api/Lanes/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteLane( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var lane = await _context.Lanes.SingleOrDefaultAsync( m => m.LaneId == id );
            if (lane == null) {
                return NotFound( );
            }

            _context.Lanes.Remove( lane );
            await _context.SaveChangesAsync( );

            return Ok( lane );
        }

        private bool LaneExists( int id ) {
            return _context.Lanes.Any( e => e.LaneId == id );
        }
    }
}