using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/BuildItens" )]
    public class BuildItensController : Controller {
        private readonly EntidadesContexto _context;

        public BuildItensController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/BuildItens
        [HttpGet]
        public IEnumerable<BuildItem> GetBuildItens() {
            return _context.BuildItens;
        }

        // GET: api/BuildItens/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetBuildItem( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildItem = await _context.BuildItens.SingleOrDefaultAsync( m => m.BuildItemId == id );

            if (buildItem == null) {
                return NotFound( );
            }

            return Ok( buildItem );
        }

        // PUT: api/BuildItens/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutBuildItem( [FromRoute] int id, [FromBody] BuildItem buildItem ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != buildItem.BuildItemId) {
                return BadRequest( );
            }

            _context.Entry( buildItem ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!BuildItemExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/BuildItens
        [HttpPost]
        public async Task<IActionResult> PostBuildItem( [FromBody] BuildItem buildItem ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.BuildItens.Add( buildItem );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetBuildItem", new { id = buildItem.BuildItemId }, buildItem );
        }

        // DELETE: api/BuildItens/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteBuildItem( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var buildItem = await _context.BuildItens.SingleOrDefaultAsync( m => m.BuildItemId == id );
            if (buildItem == null) {
                return NotFound( );
            }

            _context.BuildItens.Remove( buildItem );
            await _context.SaveChangesAsync( );

            return Ok( buildItem );
        }

        private bool BuildItemExists( int id ) {
            return _context.BuildItens.Any( e => e.BuildItemId == id );
        }
    }
}