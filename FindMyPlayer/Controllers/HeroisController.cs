using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Herois" )]
    public class HeroisController : Controller {
        private readonly EntidadesContexto _context;

        public HeroisController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Herois
        [HttpGet]
        public IEnumerable<Heroi> GetHerois() {
            return _context.Herois;
        }

        // GET: api/Herois/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetHeroi( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var heroi = await _context.Herois.SingleOrDefaultAsync( m => m.HeroiId == id );

            if (heroi == null) {
                return NotFound( );
            }

            return Ok( heroi );
        }

        // PUT: api/Herois/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutHeroi( [FromRoute] int id, [FromBody] Heroi heroi ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != heroi.HeroiId) {
                return BadRequest( );
            }

            _context.Entry( heroi ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!HeroiExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Herois
        [HttpPost]
        public async Task<IActionResult> PostHeroi( [FromBody] Heroi heroi ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Herois.Add( heroi );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetHeroi", new { id = heroi.HeroiId }, heroi );
        }

        // DELETE: api/Herois/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteHeroi( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var heroi = await _context.Herois.SingleOrDefaultAsync( m => m.HeroiId == id );
            if (heroi == null) {
                return NotFound( );
            }

            _context.Herois.Remove( heroi );
            await _context.SaveChangesAsync( );

            return Ok( heroi );
        }

        private bool HeroiExists( int id ) {
            return _context.Herois.Any( e => e.HeroiId == id );
        }
    }
}