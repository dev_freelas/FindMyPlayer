using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FindMyPlayer.Models;

namespace FindMyPlayer.Controllers {
    [Produces( "application/json" )]
    [Route( "api/Itens" )]
    public class ItensController : Controller {
        private readonly EntidadesContexto _context;

        public ItensController() {
            _context = new EntidadesContexto( );
        }

        // GET: api/Itens
        [HttpGet]
        public IEnumerable<Item> GetItens() {
            return _context.Itens;
        }

        // GET: api/Itens/5
        [HttpGet( "{id}" )]
        public async Task<IActionResult> GetItem( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var item = await _context.Itens.SingleOrDefaultAsync( m => m.ItemId == id );

            if (item == null) {
                return NotFound( );
            }

            return Ok( item );
        }

        // PUT: api/Itens/5
        [HttpPut( "{id}" )]
        public async Task<IActionResult> PutItem( [FromRoute] int id, [FromBody] Item item ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            if (id != item.ItemId) {
                return BadRequest( );
            }

            _context.Entry( item ).State = EntityState.Modified;

            try {
                await _context.SaveChangesAsync( );
            } catch (DbUpdateConcurrencyException) {
                if (!ItemExists( id )) {
                    return NotFound( );
                } else {
                    throw;
                }
            }

            return NoContent( );
        }

        // POST: api/Itens
        [HttpPost]
        public async Task<IActionResult> PostItem( [FromBody] Item item ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            _context.Itens.Add( item );
            await _context.SaveChangesAsync( );

            return CreatedAtAction( "GetItem", new { id = item.ItemId }, item );
        }

        // DELETE: api/Itens/5
        [HttpDelete( "{id}" )]
        public async Task<IActionResult> DeleteItem( [FromRoute] int id ) {
            if (!ModelState.IsValid) {
                return BadRequest( ModelState );
            }

            var item = await _context.Itens.SingleOrDefaultAsync( m => m.ItemId == id );
            if (item == null) {
                return NotFound( );
            }

            _context.Itens.Remove( item );
            await _context.SaveChangesAsync( );

            return Ok( item );
        }

        private bool ItemExists( int id ) {
            return _context.Itens.Any( e => e.ItemId == id );
        }
    }
}